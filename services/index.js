const calculateSipStepUp = inputData => {
    const { monthlySavings, yearlyIncrement, investmentPeriod, rateOfReturn } = inputData
    const months = investmentPeriod * 12
    const rate = rateOfReturn / 12

    let graph = []
    let sipStepUpSavings = monthlySavings
    let sipGrowth = 0
    let sipStepUpGrowth = 0
    let totalInvestmentTillDate = 0;

    for (let i = 1; i <= months; i++) {
        sipGrowth += monthlySavings * Math.pow(1 + rate / 100, i)

        if (i % 12 == 1 && i != 1) sipStepUpSavings=sipStepUpSavings+(sipStepUpSavings*(yearlyIncrement /100)) ;
        sipStepUpGrowth += sipStepUpSavings * Math.pow(1 + rate / 100, i)
        totalInvestmentTillDate += sipStepUpSavings;
        graph.push({
            month: i,
            sip: Math.floor(sipGrowth),
            sipStepUp: Math.floor(sipStepUpGrowth),
            totalInvestmentTillDate : Math.floor(totalInvestmentTillDate);
        })
    }

    const data = {
        monthlySavings,
        investmentPeriod,
        yearlyIncrement,
        totalSipStepUpAmount: Math.floor(sipStepUpGrowth),
        graph
    }

    return data
}

module.exports = { calculateSipStepUp }
